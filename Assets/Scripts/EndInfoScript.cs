﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// EndInfoScript is used to store and transport the ending information from the main game to the game over screen.
/// </summary>
public class EndInfoScript : MonoBehaviour {

    private int movesTaken;
    private List<Item> inventory;
    private Feature escapeRoute;

    /// <summary>
    /// Awake makes this gameobject not be destroyed on scene load.
    /// </summary>
    void Awake () {
        DontDestroyOnLoad(transform.gameObject);
    }

    /// <summary>
    /// Get/Set for moves taken.
    /// </summary>
    public int MovesTaken
    {
        get { return movesTaken; }
        set { movesTaken = value; }
    }

    /// <summary>
    /// Get/Set for inventory item list.
    /// </summary>
    public List<Item> Inventory
    {
        get { return inventory; }
        set { inventory = value; }
    }

    /// <summary>
    /// Get/Set for escape route feature.
    /// </summary>
    public Feature EscapeRoute
    {
        get { return escapeRoute; }
        set { escapeRoute = value; }
    }
    
	
}
