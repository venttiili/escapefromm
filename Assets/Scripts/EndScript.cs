﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// EndScript handles the game over screen.
/// </summary>
public class EndScript : MonoBehaviour {

    private Text mainText;
    private EndInfoScript endInfo;
    private Button quit;
    private Button replay;
    private GameObject wardenEnd;

    /// <summary>
    /// Start initializes the end screen.
    /// </summary>
    void Start () {
        mainText = GameObject.Find("MainText").GetComponent<Text>();
        endInfo = GameObject.Find("EndInfo(Clone)").GetComponent<EndInfoScript>();
        quit = GameObject.Find("butQuit").GetComponent<Button>();
        replay = GameObject.Find("butReplay").GetComponent<Button>();
        wardenEnd = GameObject.Find("WardenEnd");
        wardenEnd.SetActive(false);
        setMainText();

        quit.onClick.AddListener(() => Application.Quit());
        replay.onClick.AddListener(() => replayGame());
    }

    /// <summary>
    /// replayGame loads the main game for replay.
    /// </summary>
    private void replayGame()
    {
        Destroy(endInfo.gameObject);
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// setMainText sets the end text to display information about the ending the player got.
    /// </summary>
    private void setMainText()
    {
        if (endInfo.EscapeRoute.GetName().Equals("Ventilation duct"))
        {
            mainText.text = "You managed to escape the prison using the screwdriver. Freedom must feel great after a long time but you never faced the prison warden. You never had your revenge...";
        }
        else if (endInfo.EscapeRoute.GetName().Equals("Narrow hole"))
        {
            mainText.text = "You managed to escape the prison using the shovel. Freedom must feel great after a long time but you never faced the prison warden. You never had your revenge...";
        }
        else if (endInfo.EscapeRoute.GetName().Equals("Warden"))
        {
            wardenEnd.SetActive(true);
            mainText.text = "You did it! After all this time in prison you have finally taken your revenge on the warden and escaped through the back door. It was too long of a time in the prison for a murder of your wife, a murder you didn’t even commit. You found some papers on the warden’s desk that he wrote confessing everything. Some cheesy guy might even say that justice has been served.";
        }
        else
        {
            mainText.text = "You escaped through unknown means.";
        }

        mainText.text += "\n\nYou moved " + endInfo.MovesTaken + " times.";
        mainText.text += "\n\nYou escaped with: " + getInventoryNames() + ".";
    }

    /// <summary>
    /// getInventoryNames gets the names of all the items player had when he escaped.
    /// </summary>
    private string getInventoryNames()
    {
    
        string itemDescs = "";
        if (endInfo.Inventory.Count == 0)
        {
            return itemDescs;
        }
        else
        {
            itemDescs = "";
            itemDescs += "<color=#00ff00>";
            foreach (Item i in endInfo.Inventory)
            {
                itemDescs += i.GetName() + ", ";
            }

            itemDescs = itemDescs.Substring(0, itemDescs.Length - 2);
            itemDescs += "</color>";
            return itemDescs;
        }
    }
	
}
