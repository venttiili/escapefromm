﻿using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// GameMap creates and handles the game map and the minimap and all items and features contained within.
/// </summary>
public class GameMap : MonoBehaviour {

    private Area[,] gameMap;
    public readonly int MapX = 9, MapY = 11;
    private List<Character> allCharacters;
    private Transform mapSprites;  // For map display
    private SpriteRenderer[,] mapSpriteArray;
    public GameObject MapBlockPrefab;
    private Player player;


    /// <summary>
    /// LinkPlayer links the Player object with the GameMap object at the start of the game.
    /// </summary>
    /// <param name="p">The Player object used in the game.</param>
    public void LinkPlayer(Player p)
    {
        this.player = p;
    }

    /// <summary>
    /// GetDepiction returns a string describing the given area and it's nearby locations.
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>String describing an area and it's nearby locations.</returns>
    public string GetDepiction(int x, int y)
    {
        string desc = "";
        desc = gameMap[x, y].GetDescription();
        desc += "\n" + getDirectionNames(x, y);
        return desc;
    }

    /// <summary>
    /// getDirectionNames returns a string describing adjacent locations of the given location.
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>String describing the adjacent locations.</returns>
    private string getDirectionNames(int x, int y)
    {
        string dirNames = "";
        {
            dirNames += "\n";
            if (y+1 < MapY && (gameMap[x,y+1] != null))
            {
                dirNames += "<color=#00E6FF>North:</color> " + gameMap[x, y + 1].GetName() + ". ";
            }
            if (x + 1 < MapX && gameMap[x+1, y] != null)
            {
                dirNames += "<color=#00E6FF>East:</color> " + gameMap[x+1, y].GetName() + ". ";
            }
            if (y - 1 >= 0 && gameMap[x, y - 1] != null)
            {
                dirNames += "<color=#00E6FF>South:</color> " + gameMap[x, y - 1].GetName() + ". ";
            }
            if (x - 1 >= 0 && gameMap[x-1, y] != null)
            {
                dirNames += "<color=#00E6FF>West:</color> " + gameMap[x-1, y].GetName() + ". ";
            }
            return dirNames;
        }
    }

    /// <summary>
    /// AddCharacter adds the given character to the given location.
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <param name="c">Character object to add.</param>
    public void AddCharacter(int x, int y, Character c)
    {
        allCharacters.Add(c);
        gameMap[x, y].AddCharacter(c);
        c.SetLocation(x, y);
        if (c is Player)
        {
            gameMap[x, y].PlayerVisited();
            mapSpriteArray[x, y].gameObject.SetActive(true);
            mapSpriteArray[x, y].color = Color.green;
            checkMapBlockAdjacents(x, y);
        }
    }

    /// <summary>
    /// MoveCharacter attempts to move character to the given location. Returns an empty string if the move is successful or an error message if the move is unsuccessful.
    /// </summary>
    /// <param name="c">Character to move.</param>
    /// <param name="x">X-coordinate of the target area.</param>
    /// <param name="y">Y-coordinate of the target area.</param>
    /// <returns>String describing the reason for failure, or an empty string if the move is successful.</returns>
    public string MoveCharacter(Character c, int x, int y)  // Return string telling if we can move
    {
        string blocked = checkBlockedMovement(c, x, y);
        if (gameMap[x, y] == null)
        {
            return "There is nothing in that direction.";
        }
        else if (!blocked.Equals(""))
        {
            return blocked;
        }
        
        foreach (Character chara in allCharacters)
        {
            if (chara == c)
            {
                int[] oldLoc = c.GetLocation();
                gameMap[x, y].AddCharacter(c);
                gameMap[oldLoc[0], oldLoc[1]].RemoveCharacter(c);
                if ( c is Player)
                {
                    
                    gameMap[x, y].PlayerVisited();
                    mapSpriteArray[x, y].gameObject.SetActive(true);
                    
                    // Handle the map colors
                    mapSpriteArray[oldLoc[0], oldLoc[1]].color = Color.white;
                    mapSpriteArray[x, y].color = Color.green;
                    checkMapBlockAdjacents(x,y);
                }
            }
            else
            {
                Debug.Log("Could not find character: " + c);
            } 
        }
        return "";

    }

    /// <summary>
    /// checkBlockedMovement checks if a feature or a locked door blocks the movement forward.
    /// </summary>
    /// <param name="c">Character to move.</param>
    /// <param name="x">X-coordinate of the target area.</param>
    /// <param name="y">Y-coordinate of the target area.</param>
    /// <returns>String describing the reason for failure, or an empty string if the move is possible.</returns>
    private string checkBlockedMovement(Character c, int x, int y)
    {
        string canMove = "";   // Return empty string if we cant move
        int[] pLoc = c.GetLocation();
        List<Feature> areaFeatures = gameMap[pLoc[0], pLoc[1]].GetFeatureList();
        if (pLoc[0] - x == 0)
        {
            if (pLoc[1] < y)
            {
                foreach (Feature f in areaFeatures)
                {
                    if (f is LockedDoor)
                    {
                        LockedDoor l = (LockedDoor)f;
                        if (l.BlocksMoveNorth)
                        {
                            return l.GetName() + " blocks your movement to north.";
                        }
                    }
                }
            }
            else
            {
                foreach (Feature f in areaFeatures)
                {
                    
                    if (f is LockedDoor)
                    {
                        LockedDoor l = (LockedDoor)f;
                        if (l.BlocksMoveSouth)
                        {
                            return l.GetName() + " blocks your movement to south.";
                        }
                    }
                }
            }
        }
        else
        {
            if (pLoc[0] < x)
            {
                foreach (Feature f in areaFeatures)
                {
                    if (f is LockedDoor)
                    {
                        LockedDoor l = (LockedDoor)f;
                        if (l.BlocksMoveEast)
                        {
                            return l.GetName() + " blocks your movement to east.";
                        }
                    }
                }
            }
            else
            {
                foreach (Feature f in areaFeatures)
                {
                    if (f is LockedDoor)
                    {
                        LockedDoor l = (LockedDoor)f;
                        if (l.BlocksMoveWest)
                        {
                            return l.GetName() + " blocks your movement to west.";
                        }
                    }
                }
            }
        }

        return canMove;
    }

    /// <summary>
    /// checkMapBlockAdjacents handles minimap colors and sprite activation when player moves.
    /// </summary>
    /// <param name="x">X-coordinate of the current area.</param>
    /// <param name="y">Y-coordinate of the current area.</param>
    private void checkMapBlockAdjacents(int x, int y)
    {
        if (x-1 >= 0 && gameMap[x-1,y] != null)
        {
            if (!gameMap[x - 1, y].HasPlayerSeen())
            {
                mapSpriteArray[x - 1, y].gameObject.SetActive(true);
                mapSpriteArray[x - 1, y].color = Color.gray;
            }
        }
        if (x + 1 < MapX && gameMap[x + 1, y] != null)
        {
            if (!gameMap[x + 1, y].HasPlayerSeen())
            {
                mapSpriteArray[x + 1, y].gameObject.SetActive(true);
                mapSpriteArray[x + 1, y].color = Color.gray;
            }
        }
        if (y - 1 >= 0 && gameMap[x, y-1] != null)
        {
            if (!gameMap[x, y - 1].HasPlayerSeen())
            {
                mapSpriteArray[x, y - 1].gameObject.SetActive(true);
                mapSpriteArray[x, y - 1].color = Color.gray;
            }
        }
        if (y + 1 < MapY && gameMap[x, y + 1] != null)
        {
            if (!gameMap[x, y + 1].HasPlayerSeen())
            {
                mapSpriteArray[x, y + 1].gameObject.SetActive(true);
                mapSpriteArray[x, y + 1].color = Color.gray;
            }
        }
    }

    /// <summary>
    /// GetBG returns the area sprite from given location
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>Sprite of the area.</returns>
    public Sprite GetBG(int x, int y)
    {
        return gameMap[x, y].GetBG();
    }

    /// <summary>
    /// NewMap creates a new game map array, creates all the area, item, feature and character objects.
    /// </summary>
    public void NewMap()
    {
        mapSprites = GameObject.Find("MapSprites").GetComponent<Transform>();
        allCharacters = new List<Character>();
        gameMap = new Area[MapX, MapY];
        mapSpriteArray = new SpriteRenderer[MapX, MapY];
        GameObject tempMapBlock;

        // Map

        gameMap[1, 7] = new Area("My own cell", "A cell that has become my home.It does not have much in it.A bunk bed on the wall and a sink on another one.",
            Resources.Load<Sprite>("Textures/oma_selli"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites);
        mapSpriteArray[1, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (1 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        gameMap[1, 5] = new Area("A cell on the west wall", "This cell is very similar to my own. It has a bed and a sink but not much else in it. Boring...",
            Resources.Load<Sprite>("Textures/cell_west_bad"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[1, 5] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (1 * 0.65f), -4.5f + (5 * 0.65f), 0.0f);

        gameMap[1, 3] = new Area("A nice cell on the west wall", "This cell is somewhat similar to my own. It has a bed and a sink but also a bookshelf and a small TV. It is also close to the yard. Whoever lives in this cell must have some connections.",
            Resources.Load<Sprite>("Textures/cell_west_good"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[1, 3] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (1 * 0.65f), -4.5f + (3 * 0.65f), 0.0f);

        gameMap[2, 1] = new Area("Yard", "South part of the prison yard. You can see the freedom that lies outside the prison. Oh, what a lovely flowerbed!",
            Resources.Load<Sprite>("Textures/yard_bottom"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 1] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (1 * 0.65f), 0.0f);

        gameMap[2, 2] = new Area("Yard", "North part of the prison yard. Prisoners can come here to enjoy some fresh air.",
            Resources.Load<Sprite>("Textures/yard_top"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 2] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (2 * 0.65f), 0.0f);

        gameMap[2, 3] = new Area("Hallway", "Hallway with an isolation cell to the east, another prisoner’s cell to the west and entrance to the yard to the south. Some maniac is currently locked in the isolation cell.",
            Resources.Load<Sprite>("Textures/kaytava_verti_right_downnice_left"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 3] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (3 * 0.65f), 0.0f);

        gameMap[2, 4] = new Area("Uninteresting hallway", "Another uninteresting hallway. So plain...",
            Resources.Load<Sprite>("Textures/kaytava_verti"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 4] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (4 * 0.65f), 0.0f);

        gameMap[2, 5] = new Area("Hallway", "Hallway with other prisoners’ cells in both sides",
            Resources.Load<Sprite>("Textures/kaytava_verti_right_left"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 5] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (5 * 0.65f), 0.0f);

        gameMap[2, 6] = new Area("Hallway", "An uninteresting hallway. It’s so empty and quiet you can almost hear your own thoughts.",
            Resources.Load<Sprite>("Textures/kaytava_verti"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 6] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (6 * 0.65f), 0.0f);

        gameMap[2, 7] = new Area("Hallway", "Hallway with your own cell to the west, recreational space to the north and more hallway to the east and south.",
            Resources.Load<Sprite>("Textures/kaytava_corner"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        gameMap[2, 8] = new Area("Recreational space", "Prisoners’ recreational space with various entertainment possibilities such as a pool table and a television",
            Resources.Load<Sprite>("Textures/recreational"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[2, 8] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (2 * 0.65f), -4.5f + (8 * 0.65f), 0.0f);

        gameMap[3, 3] = new Area("Isolation cell", "Isolation cell at the south end of the hallway. Very dark and scary!",
            Resources.Load<Sprite>("Textures/cell_east_isolation"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[3, 3] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (3 * 0.65f), -4.5f + (3 * 0.65f), 0.0f);

        gameMap[3, 5] = new Area("A cell on the east wall.", "Similar to my own cell but mirrored. Many counting engravings on the wall for the days spent here. Inmate must have something sharp.",
             Resources.Load<Sprite>("Textures/cell_east_engravings"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[3, 5] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (3 * 0.65f), -4.5f + (5 * 0.65f), 0.0f);

        gameMap[3, 7] = new Area("Hallway", "This hallway has a door to the cafeteria. Hallway is really restricted and the guard booth is really near.",
            Resources.Load<Sprite>("Textures/kaytava_hori_up_nicedoor"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[3, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (3 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        gameMap[3, 8] = new Area("Inmates Cafeteria", "Food is served at the left back corner.Lining starts next to the left wall.Tables are placed at the center of the room. Inmates must return the dishes at the right back corner. Between the serving route and the dishreturn point is a door to the kitchen.",
             Resources.Load<Sprite>("Textures/cafeteria"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[3, 8] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (3 * 0.65f), -4.5f + (8 * 0.65f), 0.0f);

        gameMap[3, 9] = new Area("The kitchen", "This is where the food is prepared every day.Some prisoners work here.There are many stoves and sinks and several fridges in here.There are also locked cabinets where they propably keep the appliances and the cutlery.",
            Resources.Load<Sprite>("Textures/kitchen"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[3, 9] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (3 * 0.65f), -4.5f + (9 * 0.65f), 0.0f);

        gameMap[3, 10] = new Area("The food storage", "There are many shelves stocked with large amounts of food in tins and jars.There is also a large tank of what appears to be water.There is not much fresh food in here.",
            Resources.Load<Sprite>("Textures/food_storage"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[3, 10] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (3 * 0.65f), -4.5f + (10 * 0.65f), 0.0f);

        gameMap[4, 6] = new Area("Guard booth", "Prison guard’s booth with various items that the guard needs. What a messy guy he is!",
            Resources.Load<Sprite>("Textures/guards_booth"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[4, 6] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (4 * 0.65f), -4.5f + (6 * 0.65f), 0.0f);

        gameMap[4, 7] = new Area("Hallway", "East - south - west hallway with the guard’s booth to the south and more hallway to the east and west.",
            Resources.Load<Sprite>("Textures/kaytava_hori_down_nicedoor"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[4, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (4 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        gameMap[5, 7] = new Area("Staircase", "Low lighted staircase. First floor is to the left and second floor to the right. That is one good looking staircase!",
            Resources.Load<Sprite>("Textures/staircase"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[5, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (5 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        // Second floor

        gameMap[6, 6] = new Area("Hallway", "This hallway leads vertically to the lobby and laundry room. Why is it so empty?",
            Resources.Load<Sprite>("Textures/kaytava_verti"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[6, 6] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (6 * 0.65f), -4.5f + (6 * 0.65f), 0.0f);

        gameMap[6, 7] = new Area("Hallway", "Second floor hallway starts here. Hallway has two possible directions. One heads towards the warden’s office and the other heads to the laundry room and lobby. There is also a door to the control room. Looks like the control room has a complex lock.",
            Resources.Load<Sprite>("Textures/kaytava_hori_corner_up_nicedoor"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[6, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (6 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        gameMap[6, 8] = new Area("Prison control room", "Prisons control room. At the back of the room there is a big table with couple of screens. Screens show camera surveillance from almost every room of the prison. Looks like the warden’s office has no surveillance.",
            Resources.Load<Sprite>("Textures/surveillance_room"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[6, 8] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (6 * 0.65f), -4.5f + (8 * 0.65f), 0.0f);
        
        gameMap[6, 5] = new Area("Hallway", "Second floor hallway ends here. There are two doors. One leads to the lobby and one to the laundry room. Lobby’s door is highly secured but the laundry room’s door is open.",
            Resources.Load<Sprite>("Textures/kaytava_verti_rightnice_downnice"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[6, 5] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (6 * 0.65f), -4.5f + (5 * 0.65f), 0.0f);

        gameMap[6, 4] = new Area("Prison entry", "Way out of the prison. Highly restricted room with around the clock surveillance. No way of escaping through this door.",
            Resources.Load<Sprite>("Textures/lobby"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[6, 4] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (6 * 0.65f), -4.5f + (4 * 0.65f), 0.0f);

        gameMap[7, 5] = new Area("Laundry room", "A room where the prison staff washes clothes. This is a known place for trading illegal items.",
            Resources.Load<Sprite>("Textures/laundry_room"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[7, 5] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (7 * 0.65f), -4.5f + (5 * 0.65f), 0.0f);
        
        gameMap[7, 7] = new Area("Hallway", "End of the hallway with the notorious prison warden’s room to the east and more hallway to the west. Be careful of the warden! There’s even a warning sign.",
            Resources.Load<Sprite>("Textures/kaytava_hori_rightwarden"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[7, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (7 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        gameMap[8, 7] = new Area("Warden’s room", "This is the warden’s room. He looks mad! Now is your chance to make things right and KILL him.",
            Resources.Load<Sprite>("Textures/wardens_room"));
        tempMapBlock = (GameObject)Instantiate(MapBlockPrefab, Vector3.zero, Quaternion.identity);
        tempMapBlock.transform.SetParent(mapSprites.transform);
        mapSpriteArray[8, 7] = tempMapBlock.GetComponent<SpriteRenderer>();
        tempMapBlock.transform.localPosition = new Vector3(-5.5f + (8 * 0.65f), -4.5f + (7 * 0.65f), 0.0f);

        // Items & Features

        Item lockpick = new Item("Lockpick", "Lockpick", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/Tiirikka"));

        Item rustyKey = new Item("Rusty cell key", "Rusty cell key for some first floor cell", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/avain1"));

        Item guardRoomKey = new Item("Guard room key", "Guard room key", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/avain1"));
        gameMap[6, 4].AddItem(guardRoomKey);

        Item controlRoomKey = new Item("Bundle of keys", "A few keys in a keyring", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/avain1"));

        Item wardenRoomKey = new Item("Warden room key", "Warden room key", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/avain1"));

        Item screwDriver = new Item("Screwdriver", "Old screwdriver", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/meisseli"));
        gameMap[4, 6].AddItem(screwDriver);

        Item cartonOfCigs = new Item("Carton of Cigarettes", "Carton of Cigarettes", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/kartonki"));

        Item goldBar = new Item("Gold bar", "Dirty gold bar", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/harkko"));
        
        Item knife = new Item("Knife", "Knife Desc", 3,
            Resources.Load<Sprite>("Textures/SmallSprite/puukko"));
        gameMap[3, 5].AddItem(knife);

        Item axe = new Item("Axe", "Small axe", 6,
           Resources.Load<Sprite>("Textures/SmallSprite/kirves"));
        gameMap[3, 10].AddItem(axe);

        Item revolver = new Item("Revolver", "Easily concealed revolver", 12,
           Resources.Load<Sprite>("Textures/SmallSprite/revolveri"));

        Item shovel = new Item("Shovel", "Small rusty shovel", 2,
            Resources.Load<Sprite>("Textures/SmallSprite/lapio"));


        Feature narrowHole = new Feature("Narrow hole", "Narrow hole dug under the bed");
        narrowHole.EscapeRoute = true;
        gameMap[1, 5].AddFeature(narrowHole);
        shovel.AddUse(narrowHole);

        Feature airVent = new Feature("Ventilation duct", "Ventilation duct");
        airVent.EscapeRoute = true;
        gameMap[3, 10].AddFeature(airVent);
        screwDriver.AddUse(airVent);

        Feature dirtMound = new Feature("Small dirt mound", "You uncover something under the mound of dirt!");
        shovel.AddUse(dirtMound);
        dirtMound.SetGiveItem(goldBar);
        gameMap[2, 1].AddFeature(dirtMound);

        // Doors

        LockedDoor myCellDoor = new LockedDoor("Locked door", "Unlocked door", true, false, true, false, false);
        lockpick.AddUse(myCellDoor);
        gameMap[1, 7].AddItem(lockpick);
        gameMap[1, 7].AddFeature(myCellDoor);

        LockedDoor guardRoomDoor = new LockedDoor("Guard room door", "Unlocked door", true, false, false, true, false);
        guardRoomKey.AddUse(guardRoomDoor);
        gameMap[4, 7].AddFeature(guardRoomDoor);

        LockedDoor isolationCellDoor = new LockedDoor("Isolation cell door", "Unlocked cell door", true, false, true, false, false);
        rustyKey.AddUse(isolationCellDoor);
        gameMap[2, 3].AddFeature(isolationCellDoor);

        LockedDoor controlRoomDoor = new LockedDoor("Control room door", "Unlocked door", true, true, false, false, false);
        controlRoomKey.AddUse(controlRoomDoor);
        gameMap[6, 7].AddFeature(controlRoomDoor);

        LockedDoor lobbyRoomDoor = new LockedDoor("Lobby door", "Unlocked door", true, false, false, true, false);
        lockpick.AddUse(lobbyRoomDoor);
        gameMap[6, 5].AddFeature(lobbyRoomDoor);

        LockedDoor foodStorageDoor = new LockedDoor("Food storage door", "Unlocked door", true, true, false, false, false);
        controlRoomKey.AddUse(foodStorageDoor);
        gameMap[3, 9].AddFeature(foodStorageDoor);

        LockedDoor wardenDoor = new LockedDoor("Warden room door", "Unlocked door", true, false, true, false, false);
        wardenRoomKey.AddUse(wardenDoor);
        gameMap[7, 7].AddFeature(wardenDoor);

        // Characters

        Character thinInmate = new Character("Thin inmate", "This inmate definitely looks weak.He is also known to be involved in item trading.", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"));
        thinInmate.AddDialogue("Greetings friend. How are you today? Still hoping to escape this place? Maybe I can help you.");
        thinInmate.SetTradingItems(rustyKey, lockpick);
        gameMap[2, 8].AddCharacter(thinInmate);

        Character inmateWithConnections = new Character("Inmate with connections", "This inmate is everybody’s friend. Even the nasty warden likes this guy. He always has pretty much everything you could possibly need.", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"));
        inmateWithConnections.AddDialogue("You want to become the second richest inmate in here? Maybe you could use the shovel out in the yard.");
        inmateWithConnections.SetTradingItems(shovel, cartonOfCigs);
        gameMap[1, 3].AddCharacter(inmateWithConnections);

        Character tradingInmate = new Character("Trading inmate", "This inmate is a known item trader. He’s got what you need but it usually comes with a heavy price!", 0,
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"));
        tradingInmate.AddDialogue("You want to take revenge on the warden? You need my wares!");
        tradingInmate.SetTradingItems(revolver, goldBar);
        gameMap[7, 5].AddCharacter(tradingInmate);

        Character isolationInmate = new Character("Isolation inmate", "This inmate looks like he’s been in this isolation cell for too long. You might need some kind of weapon to defeat this kind of maniac.", 2,
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/inmate"));
        isolationInmate.Hostile = true;
        isolationInmate.AddItem(controlRoomKey);
        gameMap[3, 3].AddCharacter(isolationInmate);
        
        Character controlRoomGuard = new Character("Control room guard", "This guard is not like the other ones you have seen. You surely need a strong weapon to defeat a guard this big.", 6,
            Resources.Load<Sprite>("Textures/SmallSprite/guard"),
            Resources.Load<Sprite>("Textures/SmallSprite/guard_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/guard_rightside"));
        controlRoomGuard.AddDialogue("You are not allowed to be here.");
        controlRoomGuard.Hostile = true;
        controlRoomGuard.AddItem(cartonOfCigs);
        gameMap[6, 8].AddCharacter(controlRoomGuard);

        Character wardenGuard = new Character("Warden’s guard", "This guard is the toughest one in this prison.He is personally responsible of warden’s safety.You can’t beat him without a gun.", 12,
            Resources.Load<Sprite>("Textures/SmallSprite/guard"),
            Resources.Load<Sprite>("Textures/SmallSprite/guard_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/guard_rightside"));
        wardenGuard.AddDialogue("Get lost.");
        wardenGuard.Hostile = true;
        wardenGuard.AddItem(wardenRoomKey);
        gameMap[7, 7].AddCharacter(wardenGuard);

        Character warden = new Character("Warden", "This is the prison’s very own warden. Looks like he’s ready to be shot down!", 12,
            Resources.Load<Sprite>("Textures/SmallSprite/warden"),
            Resources.Load<Sprite>("Textures/SmallSprite/warden_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/warden"));
        warden.AddDialogue("You!?");
        warden.Hostile = true;
        gameMap[8, 7].AddCharacter(warden);
        
        initMapDisplay(); // Do this last
    }

    /// <summary>
    /// initMapDisplay initializes the minimap at the start of the game.
    /// </summary>
    private void initMapDisplay()
    {
        for (int i = 0; i < MapX; i++)
        {
            for (int j = 0; j < MapY; j++)
            {
                if (gameMap[i,j] != null && !gameMap[i,j].HasPlayerSeen())
                {
                    mapSpriteArray[i, j].gameObject.SetActive(false);
                    //Debug.Log("X: " + i + " - Y: " + j);
                }
            }
        }
    }

    /// <summary>
    /// GetItemFromArea returns one item from given location or null if none are present
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>Item the player can pick up.</returns>
    public Item GetItemFromArea(int x, int y)
    {
        return gameMap[x, y].GetOneItem();
        //return gameMap[x, y].GetItemList();
    }

    /// <summary>
    /// GetAllItemsFromArea returns a list of all items in a given area
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>List of all items in the area.</returns>
    public List<Item> GetAllItemsFromArea(int x, int y)
    {
        return gameMap[x, y].GetItemList();
    }

    /// <summary>
    /// GetPersonsAt returns a list of all the characters in a given area
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>List of all characters in the area.</returns>
    public List<Character> GetPersonsAt(int x, int y)
    {
        return gameMap[x, y].GetPersons();
    }

    /// <summary>
    /// GetNonPlayerAt returns a non player character in a given area or null if none are present
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>Non-player character in the area.</returns>
    public Character GetNonPlayerAt(int x, int y)
    {
        foreach (Character c in gameMap[x,y].GetPersons())
        {
            if (!(c == player))
            {
                return c;
            }
        }
        return null;
    }

    /// <summary>
    /// GetFeaturesAt returns the list of all features in a given area.
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    /// <returns>List of all features in the area.</returns>
    public List<Feature> GetFeaturesAt(int x, int y)
    {
        return gameMap[x, y].GetFeatureList();
    }

    /// <summary>
    /// RemoveCharacter removes the given character in the given area.
    /// </summary>
    /// <param name="c">The character to remove.</param>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    public void RemoveCharacter(Character c, int x, int y)
    {
        gameMap[x, y].RemoveCharacter(c);
    }
    
}
