﻿using UnityEngine;
using System.Collections;

/// <summary>
/// LockedDoor inherits Feature and is used for all the locked door features in game.
/// </summary>
public class LockedDoor : Feature {

    
    private bool blocksMoveNorth;
    private bool blocksMoveEast;
    private bool blocksMoveSouth;
    private bool blocksMoveWest;

    /// <summary>
    /// LockedDoor constructor takes seven arguments.
    /// </summary>
    /// <param name="name">Name of the feature. Short string.</param>
    /// <param name="description">Description of the feature.</param>
    /// <param name="locked">Is this door locked.</param>
    /// <param name="blockN">Stops movement to north if locked.</param>
    /// <param name="blockE">Stops movement to east if locked.</param>
    /// <param name="blockS">Stops movement to south if locked.</param>
    /// <param name="blockW">Stops movement to west if locked.</param>
    public LockedDoor(string name, string description, bool locked, bool blockN, bool blockE,
        bool blockS, bool blockW):base(name,description)
    {
        IsLocked = locked;
        this.blocksMoveNorth = blockN;
        this.blocksMoveEast = blockE;
        this.blocksMoveSouth = blockS;
        this.blocksMoveWest = blockW;
    }

    /// <summary>
    /// Get/Set for blocksMoveNorth.
    /// </summary>
    public bool BlocksMoveNorth
    {
        get { return blocksMoveNorth; }
        set { blocksMoveNorth = value; }
    }

    /// <summary>
    /// Get/Set for blocksMoveEast.
    /// </summary>
    public bool BlocksMoveEast
    {
        get { return blocksMoveEast; }
        set { blocksMoveEast = value; }
    }

    /// <summary>
    /// Get/Set for blocksMoveSouth.
    /// </summary>
    public bool BlocksMoveSouth
    {
        get { return blocksMoveSouth; }
        set { blocksMoveSouth = value; }
    }

    /// <summary>
    /// Get/Set for blocksMoveWest.
    /// </summary>
    public bool BlocksMoveWest
    {
        get { return blocksMoveWest; }
        set { blocksMoveWest = value; }
    }

    /// <summary>
    /// UseFeature unlocks this door if locked.
    /// </summary>
    /// <returns>Description of what happens.</returns>
    public override string UseFeature()
    {
        if (IsLocked)
        {
            IsLocked = false;
            BlocksMoveNorth = false;
            BlocksMoveEast = false;
            BlocksMoveSouth = false;
            BlocksMoveWest = false;
            string tempName = Name;
            Name = GetDescription();
            return tempName + " unlocks!";
        }
        else
        {
            return "This door is already unlocked!";
        }
    }

}
