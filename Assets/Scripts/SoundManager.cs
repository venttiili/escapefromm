﻿using UnityEngine;
using System.Collections;

/// <summary>
/// SoundManager class handles the sound playback in the game.
/// </summary>
public class SoundManager : MonoBehaviour {
    
    public AudioClip WalkSound;
    public AudioClip Defeat;
    public AudioClip Encounter;
    public AudioClip Gunshot;
    public AudioClip Map;
    public AudioClip ScrewdriverUse;
    public AudioClip ShovelUse;
    public AudioClip Take;
    public AudioClip Trade;
    public AudioClip Unlock;
    public AudioClip Victory;

    private AudioSource effect;
    private AudioSource music;

    private float muteTime = 0.0f;
    private bool musicMute = false;

    /// <summary>
    /// Start finds and stores the effect and music audio sources.
    /// </summary>
    void Start () {
        effect = this.GetComponent<AudioSource>();
        music = GameObject.Find("MusicSource").GetComponent<AudioSource>();
	}

    /// <summary>
    /// Update is used to mute music for a second.
    /// </summary>
    void Update () {
        
        if (musicMute)
        {
            muteTime += Time.deltaTime;
            if (muteTime > 0.99f)
            {
                musicMute = false;
                music.mute = false;
            }
        }
	}

    /// <summary>
    /// PlaySound plays a sound effect.
    /// </summary>
    /// <param name="sound">String name of the sound to play.</param>
    public void PlaySound(string sound)
    {
        switch (sound)
        {
            case "walk":
                effect.clip = WalkSound;
                effect.Play();
                break;
            case "defeat":
                musicMute = true;
                music.mute = true;
                muteTime = 0.0f;
                effect.clip = Defeat;
                effect.Play();
                break;
            case "encounter":
                effect.clip = Encounter;
                effect.Play();
                break;
            case "gunshot":
                music.mute = true;
                effect.clip = Gunshot;
                effect.Play();
                break;
            case "map":
                effect.clip = Map;
                effect.Play();
                break;
            case "screwdriver":
                music.mute = true;
                effect.clip = ScrewdriverUse;
                effect.Play();
                break;
            case "shovel":
                music.mute = true;
                effect.clip = ShovelUse;
                effect.Play();
                break;
            case "take":
                effect.clip = Take;
                effect.Play();
                break;
            case "trade":
                effect.clip = Trade;
                effect.Play();
                break;
            case "unlock":
                effect.clip = Unlock;
                effect.Play();
                break;
            case "victory":
                effect.clip = Victory;
                effect.Play();
                break;


            default:
                break;
        }
    }
}
