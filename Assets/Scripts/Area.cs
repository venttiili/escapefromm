﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Area is the class for a single "room" in the map.
/// </summary>
public class Area
{
    private bool seenByPlayer = false;
    private string areaName;
    private string description;
    private List<Feature> features = new List<Feature>();
    private List<Item> items = new List<Item>();
    private List<Character> persons = new List<Character>();
    private Sprite areaBackground;

    /// <summary>
    /// Area constructor takes three arguments.
    /// </summary>
    /// <param name="areaName">Name of the area. Short string.</param>
    /// <param name="description">Description of the area.</param>
    /// <param name="bg">Background sprite for the area.</param>
    public Area(string areaName, string description, Sprite bg)
    {
        this.areaName = areaName;
        this.description = description;
        this.areaBackground = bg;
    }

    /// <summary>
    /// AddFeature adds a feature to this area.
    /// </summary>
    /// <param name="newFeature">The feature to be added.</param>
    public void AddFeature(Feature newFeature)
    {
        features.Add(newFeature);
    }

    /// <summary>
    /// AddItem adds an item to this area.
    /// </summary>
    /// <param name="newItem">The item to be added.</param>
    public void AddItem(Item newItem)
    {
        items.Add(newItem);
    }

    /// <summary>
    /// GetDescription returns the description and listing of items, features and characters in this area.
    /// </summary>
    /// <returns>Description of this area.</returns>
    public string GetDescription()
    {
        string desc = this.description;
        desc += getFeatureNames();
        desc += getItemNames();
        desc += getCharacterNames();
        return desc;
    }

    /// <summary>
    /// getFeatureNames returns a string listing the names of the features in this area, if any.
    /// </summary>
    /// <returns>Names of the features.</returns>
    private string getFeatureNames()
    {
        string featDescs = "";
        if (features.Count == 0)
        {
            return featDescs;
        }
        else
        {
            featDescs = "\n\n";
            featDescs += "You notice in this area: <color=#ffff00>";
            foreach (Feature f in features)
            {
                featDescs += f.GetName() + ", ";
            }
            featDescs = featDescs.Substring(0, featDescs.Length-2);
            featDescs += "</color>.";
            return featDescs;
        }
    }

    /// <summary>
    /// getItemNames returns a string listing the names of the items in this area, if any.
    /// </summary>
    /// <returns>Names of the items.</returns>
    private string getItemNames()
    {
        string itemDescs = "";
        if (items.Count == 0)
        {
            return itemDescs;
        }
        else
        {
            itemDescs = "\n";
            itemDescs += "These items are in this area: <color=#00ff00>";
            foreach (Item i in items)
            {
                itemDescs += i.GetName() + ", ";
            }

            itemDescs = itemDescs.Substring(0, itemDescs.Length - 2);
            itemDescs += "</color>.";
            return itemDescs;
        }
    }

    /// <summary>
    /// getCharacterNames returns a string listing the names and descriptions of the characters in this area, if any.
    /// </summary>
    /// <returns>Names and descriptions of the characters.</returns>
    private string getCharacterNames()
    {
        string characterNames = "";
        if (persons.Count <= 1)
        {
            return characterNames;
        }
        else
        {
            characterNames = "\n";
            characterNames += "Characters in this area:\n";
            foreach (Character c in persons)
            {
                if (!(c is Player))
                {
                    characterNames += "<color=#FFD87A>" + c.GetName() + "</color>. " + c.GetDescription() + "\n";
                }
            }
        }
        return characterNames;
    }

    /// <summary>
    /// GetName returns the name of this area.
    /// </summary>
    /// <returns>Name of this area.</returns>
    public string GetName()
    {
        return this.areaName;
    }

    /// <summary>
    /// AddCharacter adds a character into this area.
    /// </summary>
    /// <param name="c">The character to be added.</param>
    public void AddCharacter(Character c)
    {
        persons.Add(c);
    }

    /// <summary>
    /// RemoveCharacter removes a character from this area.
    /// </summary>
    /// <param name="c">The character to be removed.</param>
    public void RemoveCharacter(Character c)
    {
        persons.Remove(c);
    }

    /// <summary>
    /// PlayerVisited is called when player visits this area. After this method is called, this area is shown as bright gray in the minimap.
    /// </summary>
    public void PlayerVisited()
    {
        seenByPlayer = true;
    }

    /// <summary>
    /// HasPlayerSeen is used to check if player has visited this area.
    /// </summary>
    /// <returns>Boolean telling if player has visited this area.</returns>
    public bool HasPlayerSeen()
    {
        return seenByPlayer;
    }

    /// <summary>
    /// GetBG returns the sprite of this area.
    /// </summary>
    /// <returns>Sprite of this area.</returns>
    public Sprite GetBG()
    {
        return this.areaBackground;
    }

    /// <summary>
    /// GetItemList returns the list of the items in this area.
    /// </summary>
    /// <returns>List of items in this area.</returns>
    public List<Item> GetItemList()
    {
        return this.items;
    }

    /// <summary>
    /// GetOneItem returns the first item on the item list, if any, and removes the item form the list.
    /// </summary>
    /// <returns>First item on the list of items in this area.</returns>
    public Item GetOneItem()
    {
        if (items.Count == 0)
        {
            return null;
        }
        else
        {
            Item firstItem = items[0];
            items.RemoveAt(0);
            return firstItem;
        }
    }

    /// <summary>
    /// GetPersons returns the list of all characters in this area.
    /// </summary>
    /// <returns>List of characters in this area.</returns>
    public List<Character> GetPersons()
    {
        return persons;
    }

    /// <summary>
    /// GetFeatureList returns the list of features in this area.
    /// </summary>
    /// <returns>List of features in this area.</returns>
    public List<Feature> GetFeatureList()
    {
        return this.features;
    }

}