﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Character is the base class for all characters in the game.
/// </summary>
public class Character {

    private string name;
    private string description;
    private List<Item> inventory;
    private int strength;
    private int[] location;
    private Sprite[] sprites = { null, null, null };
    private Item tradeItem;
    private Item wantsInTrade;
    private string talkString = "";
    private string tradeString = "";
    private bool hostile = false;

    /// <summary>
    /// Character constructor takes six arguments.
    /// </summary>
    /// <param name="name">Name of the character. Short string.</param>
    /// <param name="description">Description of the character.</param>
    /// <param name="strength">Strength of the character.</param>
    /// <param name="faceDown">Sprite of the character facing down.</param>
    /// <param name="faceUp">Sprite of the character facing up.</param>
    /// <param name="faceRight">Sprite of the character facing right.</param>
    public Character(string name, string description, int strength, Sprite faceDown, Sprite faceUp, Sprite faceRight)
    {
        this.name = name;
        this.description = description;
        this.strength = strength;
        this.inventory = new List<Item>();
        this.location = new int[] { 0, 0 };   // X , Y
        sprites[0] = faceDown;
        sprites[1] = faceUp;
        sprites[2] = faceRight;
    }

    /// <summary>
    /// Get/Set for hostile boolean.
    /// </summary>
    public bool Hostile
    {
        get { return this.hostile; }
        set { hostile = value; }
    }

    /// <summary>
    /// Getter for strength value.
    /// </summary>
    public int Strength
    {
        get { return this.strength; }
        set { }
    }

    /// <summary>
    /// Getter for trade item.
    /// </summary>
    public Item TradeItem
    {
        set { }
        get { return this.tradeItem; }
    }

    /// <summary>
    /// Getter for trade demand item.
    /// </summary>
    public Item WantsInTrade
    {
        set { }
        get { return this.wantsInTrade; }
    }

    /// <summary>
    /// SetLocation sets the location of this character.
    /// </summary>
    /// <param name="x">X-coordinate of the location</param>
    /// <param name="y">Y-coordinate of the location</param>
    public void SetLocation(int x, int y)
    {
        location[0] = x;
        location[1] = y;
    }

    /// <summary>
    /// GetLocation gets the location of this character.
    /// </summary>
    /// <returns>Location of this character as int array</returns>
    public int[] GetLocation()
    {
        return this.location;
    }

    /// <summary>
    /// AddItem adds an item to this characters inventory.
    /// </summary>
    /// <param name="item">Item to be added.</param>
    public void AddItem(Item item)
    {
        inventory.Add(item);
    }

    /// <summary>
    /// GetName gets the name of this character.
    /// </summary>
    /// <returns>Name of this character.</returns>
    public string GetName()
    {
        return this.name;
    }

    /// <summary>
    /// GetDescription gets the description of this character.
    /// </summary>
    /// <returns>Description of this character.</returns>
    public string GetDescription()
    {
        return this.description;
    }

    /// <summary>
    /// ItemCount gets the number of items on this character.
    /// </summary>
    /// <returns>Number of items on this character.</returns>
    public int ItemCount()
    {
        return inventory.Count;
    }

    /// <summary>
    /// GetWholeInventory gets the whole inventory list of this character.
    /// </summary>
    /// <returns>List of inventory items.</returns>
    public List<Item> GetWholeInventory()
    {
        return this.inventory;
    }

    /// <summary>
    /// GetItemInInventory gets a item from this characters inventory.
    /// </summary>
    /// <param name="index">Index of the item in the inventory.</param>
    /// <returns>Item if it exists, otherwise null.</returns>
    public Item GetItemInInventory(int index)
    {
        if (index >= inventory.Count)
        {
            return null;
        }
        else
        {
            return inventory[index];
        }
    }

    /// <summary>
    /// AddDialogue adds dialogue string to this character.
    /// </summary>
    /// <param name="dialogue">Dialogue string to be added.</param>
    public void AddDialogue(string dialogue)
    {
        this.talkString = dialogue;
    }

    /// <summary>
    /// SetTradingItems sets the items this character wants in trade and gives in trade and creates a trade string based on them.
    /// </summary>
    /// <param name="gives">Item this character gives in trade.</param>
    /// <param name="wants">Item this character wants in trade.</param>
    public void SetTradingItems(Item gives, Item wants)
    {
        this.tradeItem = gives;
        this.wantsInTrade = wants;
        this.tradeString = "\n\nI can give you <color=#00ff00>" + gives.GetName() + "</color> for <color=#00ff00>" + wants.GetName() + "</color>."; 
    }

    /// <summary>
    /// IsTrading checks if this character is trading by checking if tradeItem has been set to an item.
    /// </summary>
    /// <returns>Is this character trading.</returns>
    public bool IsTrading()
    {
        if (tradeItem == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// TalkWith talks with this character.
    /// </summary>
    /// <returns>Dialogue and trade strings.</returns>
    public string TalkWith()
    {
        return talkString + tradeString;
    }

    /// <summary>
    /// DoTrade does the trade, setting trade items to null and trade string to an empty string.
    /// </summary>
    public void DoTrade()
    {
        this.tradeItem = null;
        this.wantsInTrade = null;
        this.tradeString = "";
    }

    /// <summary>
    /// InventoryContains checks if this character's inventory contains an item.
    /// </summary>
    /// <param name="i">Item we are checking with.</param>
    /// <returns>Does this character's inventory contain the item.</returns>
    public bool InventoryContains(Item i)
    {
        return inventory.Contains(i);
    }

    /// <summary>
    /// RemoveItemFromInventory attempts to remove an item from the inventory of this character.
    /// </summary>
    /// <param name="remove">Item to be removed.</param>
    public void RemoveItemFromInventory(Item remove)
    {
        inventory.Remove(remove);
    }

    /// <summary>
    /// GetBestWeapon looks for the best weapon in this character's inventory.
    /// </summary>
    /// <returns>Best weapon in this character's inventory, or null if none.</returns>
    public Item GetBestWeapon()
    {
        int bestIndex = 0;
        if (inventory.Count == 0)
        {
            return null;
        }
        else
        {
            for(int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].AttackPower > inventory[bestIndex].AttackPower)
                {
                    bestIndex = i;
                }
            }
            if (inventory[bestIndex].AttackPower == 0)
            {
                return null;
            }
            else
            {
                return inventory[bestIndex];
            }
        }
    }

    /// <summary>
    /// GetSprite returns the sprite for this character.
    /// </summary>
    /// <param name="facing">Facing direction of the character.</param>
    /// <returns>Sprite with the correct facing.</returns>
    public Sprite GetSprite(int facing)
    {
        if ( facing >= 0 && facing <= 2)
        {
            return sprites[facing];
        }
        else
        {
            return null;
        }
    }
	
}
