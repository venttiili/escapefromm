﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Player inherits Character and is used as the player character object.
/// </summary>
public class Player : Character {

    /// <summary>
    /// Player constructor takes six arguments.
    /// </summary>
    /// <param name="name">Name of the character. Short string.</param>
    /// <param name="description">Description of the character.</param>
    /// <param name="strength">Strength of the character.</param>
    /// <param name="faceDown">Sprite of the character facing down.</param>
    /// <param name="faceUp">Sprite of the character facing up.</param>
    /// <param name="faceRight">Sprite of the character facing right.</param>
    public Player(string name, string description, int strength, Sprite faceDown, Sprite faceUp, Sprite faceRight) :base(name, description, strength, faceDown, faceUp, faceRight)
    {
       
    }

    /// <summary>
    /// UseItem gets the list of all features the given item can be used with.
    /// </summary>
    /// <param name="item">Item of which uses we want to find.</param>
    /// <returns>List of features.</returns>
    public List<Feature> UseItem(Item item)
    {
        return item.GetUsables();
    }
    
}
