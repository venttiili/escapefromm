﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Item class is used for all the item objects in the game.
/// </summary>
public class Item
{
    private string itemName;
    private string description;
    private int attackPower;
    private List<Feature> usableFor = new List<Feature>();
    private Sprite itemSprite;

    /// <summary>
    /// Item constructor takes four arguments.
    /// </summary>
    /// <param name="itemName">Name of this item.</param>
    /// <param name="description">Description of this item.</param>
    /// <param name="attackPower">Attack power of this item.</param>
    /// <param name="itemSprite">Sprite of this item.</param>
    public Item(string itemName, string description, int attackPower, Sprite itemSprite)
    {
        this.itemName = itemName;
        this.description = description;
        this.attackPower = attackPower;
        this.itemSprite = itemSprite;
    }

    /// <summary>
    /// AttackPower gets the attackPower int value.
    /// </summary>
    public int AttackPower
    {
        get { return this.attackPower; }
        set { }
    }

    /// <summary>
    /// ItemSprite gets and sets the sprite of the item.
    /// </summary>
    public Sprite ItemSprite
    {
        get { return itemSprite; }
        set { this.itemSprite = value; }
    }

    /// <summary>
    /// AddUse makes the item able to use the given feature.
    /// </summary>
    /// <param name="useFeature">Add this feature to the list of things this item can be used with.</param>
    public void AddUse(Feature useFeature)
    {
        usableFor.Add(useFeature);
    }

    /// <summary>
    /// GetDescription returns the description string of this item.
    /// </summary>
    /// <returns>The description of the item.</returns>
    public string GetDescription()
    {
        return "Item description is: " + this.description;
    }

    /// <summary>
    /// GetName returns the name string of this item
    /// </summary>
    /// <returns>The name of the item.</returns>
    public string GetName()
    {
        return this.itemName;
    }

    /// <summary>
    /// GetUsables returns the list of all the features this item can be used with. 
    /// </summary>
    /// <returns>List of features this item can be used with.</returns>
    public List<Feature> GetUsables()
    {
        return usableFor;
    }

    /// <summary>
    /// CanUse returns boolean telling if the given feature can be used with this item.
    /// </summary>
    /// <param name="f">Check if we are usable for this feature.</param>
    /// <returns>Boolean representing if we can use this item with the feature.</returns>
    public bool CanUse(Feature f)
    {
        if (usableFor.Contains(f))
        {
            return true;
        }
        else return false;
    }

}