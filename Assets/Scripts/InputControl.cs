﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// InputControl handles getting the input from the buttons.
/// </summary>
public class InputControl : MonoBehaviour {

    private Button butNorth;
    private Button butEast;
    private Button butSouth;
    private Button butWest;

    private Button butUse;
    private Button butMap;
    private Button butTake;
    private Button butTalk;

    private GameControl gc;

    /// <summary>
    /// Start finds the buttons used in the game and adds listeners for them.
    /// </summary>
    void Start () {
        butNorth = GameObject.Find("butNorth").GetComponent<Button>();
        butEast = GameObject.Find("butEast").GetComponent<Button>();
        butSouth = GameObject.Find("butSouth").GetComponent<Button>();
        butWest = GameObject.Find("butWest").GetComponent<Button>();

        butUse = GameObject.Find("butUse").GetComponent<Button>();
        butMap = GameObject.Find("butMap").GetComponent<Button>();
        butTake = GameObject.Find("butTake").GetComponent<Button>();
        butTalk = GameObject.Find("butTalk").GetComponent<Button>();

        gc = GameObject.Find("GameCtrl").GetComponent<GameControl>();

        butNorth.onClick.AddListener(() => ButtonClicked("North"));
        butEast.onClick.AddListener(() => ButtonClicked("East"));
        butSouth.onClick.AddListener(() => ButtonClicked("South"));
        butWest.onClick.AddListener(() => ButtonClicked("West"));

        butUse.onClick.AddListener(() => ButtonClicked("Use"));
        butMap.onClick.AddListener(() => ButtonClicked("Map"));
        butTake.onClick.AddListener(() => ButtonClicked("Take"));
        butTalk.onClick.AddListener(() => ButtonClicked("Talk"));
    }

    /// <summary>
    /// ButtonClicked sends the GameControl object a string telling which button was pressed.
    /// </summary>
    /// <param name="a">Action to take.</param>
    void ButtonClicked(string a)
    {
        gc.PlayerAction(a);
    }
	
}
