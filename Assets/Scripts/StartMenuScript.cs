﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// StartMenuScript class handles the start menu screen.
/// </summary>
public class StartMenuScript : MonoBehaviour {

    private Button startButton;

    /// <summary>
    /// Start finds and stores the start button.
    /// </summary>
    void Start () {
        startButton = GameObject.Find("butStart").GetComponent<Button>();
        startButton.onClick.AddListener(() => buttonClicked("Start"));
    }

    /// <summary>
    /// buttonClicked reacts to a button press.
    /// </summary>
    /// <param name="command">String name of the command.</param>
    private void buttonClicked(string command)
    {
        switch (command)
        {
            case "Start":
                {
                    SceneManager.LoadScene(1);
                    break;
                } 
        }
    }
}
