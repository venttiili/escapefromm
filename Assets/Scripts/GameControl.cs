﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

/// <summary>
/// GameControl creates and controls the main game loop.
/// </summary>
public class GameControl : MonoBehaviour {

    private Text mainText;
    private Text dialogue;

    private Button talkYes;
    private Button talkNo;
    private Button talkBye;
    private Image tradeGives;
    private Image tradeWants;

    private Image areaBG;
    private Image textBG;
    private Image slidingAreaBG;

    private Image startEndImage;
    private Text startEndText;
    private Button startEndButton;

    private List<Character> characters;
    private GameMap gameMap;
    private Player player;
    private SoundManager soundManager;

    private Transform useButtonBase;
    private bool useMenuVisible = false;
    private bool chooseFeatureVisible = false;
    private GameObject[] useButtons;
    private GameObject useMenuBG;
    private Text useMenuText;
    
    private int movesTaken = 0;

    private List<Image> localSprites;
    private bool sliding = false;
    private Sprite lockedDoorSprite;

    public Image tempItem;
    
    private Item usingItem;
    private Character currentlyTalking;
    private int moveDirection = 0;  // Which direction player came from. N, E, S, W

    private Camera mapCamera;

    public GameObject useListButton;

    public GameObject EndInfoPrefab;

    /// <summary>
    /// Start initializes the game when the scene is loaded.
    /// </summary>
    void Start () {
        characters = new List<Character>();
        mainText = GameObject.Find("MainText").GetComponent<Text>();
        dialogue = GameObject.Find("UseMenuDialogue").GetComponent<Text>();
        talkYes = GameObject.Find("butYes").GetComponent<Button>();
        talkNo = GameObject.Find("butNo").GetComponent<Button>();
        talkBye = GameObject.Find("butBye").GetComponent<Button>();
        tradeGives = GameObject.Find("GivesImage").GetComponent<Image>();
        tradeWants = GameObject.Find("WantsImage").GetComponent<Image>();

        startEndImage = GameObject.Find("StartEndBlackImage").GetComponent<Image>();
        startEndButton = GameObject.Find("StartEndButton").GetComponent<Button>();
        startEndText = GameObject.Find("StartEndText").GetComponent<Text>();
        startEndButton.onClick.AddListener(() => startEndImage.gameObject.SetActive(false));

        talkYes.onClick.AddListener(() => talkChoise("Yes"));
        talkNo.onClick.AddListener(() => talkChoise("No"));
        talkBye.onClick.AddListener(() => talkChoise("Bye"));

        dialogue.gameObject.SetActive(false);

        mapCamera = GameObject.Find("MapCamera").GetComponent<Camera>();
        mapCamera.gameObject.SetActive(false);
        areaBG = GameObject.Find("AreaGraphic").GetComponent<Image>();
        textBG = GameObject.Find("TextBG").GetComponent<Image>();
        slidingAreaBG = GameObject.Find("SlidingAreaGraphic").GetComponent<Image>();
        gameMap = GameObject.Find("Map").GetComponent<GameMap>();
        gameMap.NewMap();

        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        useButtonBase = GameObject.Find("useButtons").GetComponent<Transform>();
        useMenuBG = GameObject.Find("UseMenuBG");
        useMenuText = GameObject.Find("UseMenuText").GetComponent<Text>();
        useMenuText.gameObject.SetActive(false);

        useMenuBG.SetActive(false);
        player = new Player("Pelaaja", "Iso Pelaaja", 1,
            Resources.Load<Sprite>("Textures/SmallSprite/player"),
            Resources.Load<Sprite>("Textures/SmallSprite/player_behind"),
            Resources.Load<Sprite>("Textures/SmallSprite/player_rightside"));
        gameMap.LinkPlayer(player);
        gameMap.AddCharacter(1, 7, player);
        moveDirection = 1;
        areaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);

        mainText.text = gameMap.GetDepiction(player.GetLocation()[0], player.GetLocation()[1]);
        lockedDoorSprite = Resources.Load<Sprite>("Textures/lockedLevel");
        localSprites = new List<Image>();
        drawLocalSprites(gameMap.GetPersonsAt(player.GetLocation()[0], player.GetLocation()[1]),
                            gameMap.GetAllItemsFromArea(player.GetLocation()[0], player.GetLocation()[1]),
                            gameMap.GetFeaturesAt(player.GetLocation()[0], player.GetLocation()[1]));
    }

    /// <summary>
    /// PlayerAction receives commands from inputs and executes the given actions.
    /// </summary>
    /// <param name="action">Action to execute in string form.</param>
    public void PlayerAction(string action)
    {
        if (!sliding)
        {
            switch (action)
            {
                case "North":
                    if (player.GetLocation()[1] + 1 < gameMap.MapY)
                    {
                        string movemsg = gameMap.MoveCharacter(player, player.GetLocation()[0], player.GetLocation()[1] + 1);
                        if (movemsg.Equals(""))
                        {
                            player.SetLocation(player.GetLocation()[0], player.GetLocation()[1] + 1);
                            mainText.text = gameMap.GetDepiction(player.GetLocation()[0], player.GetLocation()[1]);
                            slidingAreaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);
                            soundManager.PlaySound("walk");
                            moveDirection = 2;
                            StartCoroutine(transitionSlide(0));
                            movesTaken++;
                        }
                        else
                        {
                            addMessageToText(movemsg);
                        }
                    }

                    break;
                case "East":
                    if (player.GetLocation()[0] + 1 < gameMap.MapX)
                    {
                        string movemsg = gameMap.MoveCharacter(player, player.GetLocation()[0] + 1, player.GetLocation()[1]);
                        if (movemsg.Equals(""))
                        {
                            player.SetLocation(player.GetLocation()[0] + 1, player.GetLocation()[1]);
                            mainText.text = gameMap.GetDepiction(player.GetLocation()[0], player.GetLocation()[1]);
                            slidingAreaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);
                            soundManager.PlaySound("walk");
                            moveDirection = 3;
                            StartCoroutine(transitionSlide(1));
                            movesTaken++;
                        }
                        else
                        {
                            addMessageToText(movemsg);
                        }
                    }
                    break;
                case "South":
                    if (player.GetLocation()[1] - 1 >= 0)
                    {
                        string movemsg = gameMap.MoveCharacter(player, player.GetLocation()[0], player.GetLocation()[1] - 1);
                        if (movemsg.Equals(""))
                        {
                            player.SetLocation(player.GetLocation()[0], player.GetLocation()[1] - 1);
                            mainText.text = gameMap.GetDepiction(player.GetLocation()[0], player.GetLocation()[1]);
                            slidingAreaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);
                            soundManager.PlaySound("walk");
                            moveDirection = 0;
                            StartCoroutine(transitionSlide(2));
                            movesTaken++;
                        }
                        else
                        {
                            addMessageToText(movemsg);
                        }
                    }
                    break;
                case "West":
                    if (player.GetLocation()[0] - 1 >= 0)
                    {
                        string movemsg = gameMap.MoveCharacter(player, player.GetLocation()[0] - 1, player.GetLocation()[1]);
                        if (movemsg.Equals(""))
                        {
                            player.SetLocation(player.GetLocation()[0] - 1, player.GetLocation()[1]);
                            mainText.text = gameMap.GetDepiction(player.GetLocation()[0], player.GetLocation()[1]);
                            slidingAreaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);
                            soundManager.PlaySound("walk");
                            moveDirection = 1;
                            StartCoroutine(transitionSlide(3));
                            movesTaken++;
                        }
                        else
                        {
                            addMessageToText(movemsg);
                        }
                    }
                    break;
                case "Use":
                    {
                        useItem();
                        break;
                    }
                case "Map":
                    {
                        mapCamera.gameObject.SetActive(!mapCamera.gameObject.activeSelf);
                        mainText.gameObject.SetActive(!mainText.gameObject.activeSelf);
                        textBG.gameObject.SetActive(!textBG.gameObject.activeSelf);
                        soundManager.PlaySound("map");
                        break;
                    }
                case "Take":
                    {
                        takeItem(player.GetLocation()[0], player.GetLocation()[1]);
                        break;
                    }
                case "Talk":
                    {
                        talk();
                        break;
                    }
                default:
                    mainText.text = "Unknown action";
                    break;
            }
        }
    }

    /// <summary>
    /// transitionSlide coroutine slides the next screen in to view when player moves.
    /// </summary>
    /// <param name="dir">The direction of player movement.</param>
    /// <returns>IEnumerator for coroutine and yield statement.</returns>
    private IEnumerator transitionSlide(int dir)
    {
        sliding = true;
        if (dir == 0)   // North
        {
            slidingAreaBG.rectTransform.position = areaBG.transform.position + new Vector3(0.0f, 170.0f, 0);
        }
        else if (dir == 1) // East
        {
            slidingAreaBG.rectTransform.position = areaBG.transform.position + new Vector3(338.0f, 0, 0);
        }
        else if (dir == 2)  // South
        {
            slidingAreaBG.rectTransform.position = areaBG.transform.position + new Vector3(0.0f, -170.0f, 0);
        }
        else if (dir == 3)  // West
        {
            slidingAreaBG.rectTransform.position = areaBG.transform.position + new Vector3(-338.0f, 0, 0);
        }
        slidingAreaBG.gameObject.SetActive(true);
        Vector3 startPos = slidingAreaBG.rectTransform.position;
        float timeElapsed = 0.0f;
        while (timeElapsed < 0.3f )
        {

            slidingAreaBG.rectTransform.position = Vector3.Lerp(startPos,areaBG.rectTransform.position, 3.4f * timeElapsed);
            timeElapsed += Time.deltaTime;
            yield return 0;
        }
        areaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);
        slidingAreaBG.gameObject.SetActive(false);
        drawLocalSprites(gameMap.GetPersonsAt(player.GetLocation()[0], player.GetLocation()[1]),
                            gameMap.GetAllItemsFromArea(player.GetLocation()[0], player.GetLocation()[1]),
                            gameMap.GetFeaturesAt(player.GetLocation()[0], player.GetLocation()[1]));

        sliding = false;
    }

    /// <summary>
    /// drawLocalSprites draws sprites of items and characters and locked doors in the area.
    /// </summary>
    /// <param name="chars">List of characters in the area.</param>
    /// <param name="items">List of items in the area.</param>
    /// <param name="feats">List of features in the area.</param>
    private void drawLocalSprites(List<Character> chars, List<Item> items, List<Feature> feats)
    {
        foreach(Image i in localSprites)
        {
            i.gameObject.SetActive(false);
        }
        int counter = 0;
        foreach (Character c in chars)
        {
            if (localSprites.Count > counter)
            {
                localSprites[counter].gameObject.SetActive(true);
                if (c == player)
                {
                    drawPlayerSprite(localSprites[counter]);
                    counter++;
                    continue;
                }
                localSprites[counter].rectTransform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                localSprites[counter].rectTransform.localPosition = new Vector3(Random.Range(-140, 140), Random.Range(-40, 40), 0);
                localSprites[counter].sprite = c.GetSprite(0);
                counter++;
            }
            else
            {
                
                Image newLocalImg = (Image)Instantiate(tempItem, Vector3.zero, Quaternion.identity);
                newLocalImg.transform.SetParent(areaBG.transform);
                localSprites.Add(newLocalImg);
                if (c == player)
                {
                    drawPlayerSprite(newLocalImg);
                    counter++;
                    continue;
                }
                localSprites[counter].rectTransform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                newLocalImg.rectTransform.localPosition = new Vector3(Random.Range(-140, 140), Random.Range(-40, 40), 0);
                newLocalImg.sprite = c.GetSprite(0);
                counter++;
            }
        }
        foreach (Item i in items)
        {
            if (localSprites.Count > counter)
            {
                localSprites[counter].gameObject.SetActive(true);
                localSprites[counter].rectTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                localSprites[counter].rectTransform.localPosition = new Vector3(Random.Range(-140, 140), Random.Range(-40, 40), 0);
                localSprites[counter].sprite = i.ItemSprite;
                counter++;
            }
            else
            {
                Image newLocalImg = (Image)Instantiate(tempItem, Vector3.zero, Quaternion.identity);
                newLocalImg.transform.SetParent(areaBG.transform);
                localSprites.Add(newLocalImg);
                newLocalImg.rectTransform.localPosition = new Vector3(Random.Range(-140, 140), Random.Range(-40, 40), 0);
                newLocalImg.sprite = i.ItemSprite;
                counter++;
            }
        }
        foreach (Feature f in feats)
        {
            GameObject lockSpriteGO;
            if (f is LockedDoor)
            {
                LockedDoor l = f as LockedDoor;
                if (l.IsLocked)
                {
                    if (localSprites.Count > counter)
                    {
                        localSprites[counter].gameObject.SetActive(true);
                        localSprites[counter].rectTransform.localPosition = Vector3.zero;
                        localSprites[counter].rectTransform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                        localSprites[counter].sprite = lockedDoorSprite;
                        lockSpriteGO = localSprites[counter].gameObject;
                        counter++;
                    }
                    else
                    {
                        Image newLocalImg = (Image)Instantiate(tempItem, Vector3.zero, Quaternion.identity);
                        newLocalImg.transform.SetParent(areaBG.transform);
                        localSprites.Add(newLocalImg);
                        newLocalImg.rectTransform.localPosition = Vector3.zero;
                        newLocalImg.sprite = lockedDoorSprite;
                        lockSpriteGO = newLocalImg.gameObject;
                        counter++;
                    }
                    // Set the lock sprite location to what door is locked
                    if (l.BlocksMoveNorth)
                    {
                        lockSpriteGO.transform.localPosition = new Vector3(0.0f, 70.0f, 0.0f);
                    }
                    else if (l.BlocksMoveEast)
                    {
                        lockSpriteGO.transform.localPosition = new Vector3(155.0f, 0.0f, 0.0f);
                    }
                    else if (l.BlocksMoveSouth)
                    {
                        lockSpriteGO.transform.localPosition = new Vector3(0.0f, -70.0f, 0.0f);
                    }
                    else if (l.BlocksMoveWest)
                    {
                        lockSpriteGO.transform.localPosition = new Vector3(-155.0f, 0.0f, 0.0f);
                    }
                }
            }
        }
    }

    /// <summary>
    /// drawPlayerSprite draws player's sprite to the edge where he came from.
    /// </summary>
    /// <param name="spriteBase">The image where the sprite is drawn to.</param>
    private void drawPlayerSprite(Image spriteBase)
    {
        spriteBase.rectTransform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        if (moveDirection == 0)         // Came from North
        {
            spriteBase.sprite = player.GetSprite(0);
            spriteBase.rectTransform.localPosition = new Vector3(0.0f,30.0f,0.0f);
        }
        else if (moveDirection == 1)    // Came from East
        {
            spriteBase.sprite = player.GetSprite(2);
            spriteBase.rectTransform.localScale = new Vector3(-1.5f, 1.5f, 1.5f);
            spriteBase.rectTransform.localPosition = new Vector3(120.0f, 0.0f, 0.0f);
        }
        else if (moveDirection == 2)    // Came from South
        {
            spriteBase.sprite = player.GetSprite(1);
            spriteBase.rectTransform.localPosition = new Vector3(0.0f, -30.0f, 0.0f);
        }
        else if (moveDirection == 3)    // Came from West
        {
            spriteBase.sprite = player.GetSprite(2);
            spriteBase.rectTransform.localPosition = new Vector3(-120.0f, 0.0f, 0.0f);
        }
    }

    /// <summary>
    /// addMessageToText adds a new text string to the main text area, if it does not contain the text already.
    /// </summary>
    /// <param name="msg">The text string to add.</param>
    private void addMessageToText(string msg)
    {
        if (!mainText.text.Contains(msg))
        {
            mainText.text = msg + "\n\n" + mainText.text; // New text added to top
        }
    }

    /// <summary>
    /// takeItem attempts to pick up a item in an area.
    /// </summary>
    /// <param name="x">X-coordinate of the area.</param>
    /// <param name="y">Y-coordinate of the area.</param>
    private void takeItem(int x, int y)
    {
        Item areaItem = gameMap.GetItemFromArea(x, y);
        if (areaItem == null)
        {
            addMessageToText("Nothing to pick up.");
            
        }
        else
        {
            soundManager.PlaySound("take");
            player.AddItem(areaItem);
            addMessageToText("Picked up " + areaItem.GetName().ToLower() + ".");
            foreach (Image i in localSprites)
            {
                if (i.sprite == areaItem.ItemSprite)
                {
                    i.gameObject.SetActive(false);
                }
            }
            movesTaken++;
        }
    }

    /// <summary>
    /// useItem attempts to use an item in an area. Player gets to choose from items in his inventory.
    /// </summary>
    private void useItem()
    {
        int playerItems = player.ItemCount();
        if (playerItems == 0)
        {
            addMessageToText("You have nothing to use.");
        }
        
        else if (!useMenuVisible)
        {
            useMenuBG.SetActive(true);
            useMenuText.gameObject.SetActive(true);
            useMenuVisible = true;
            useMenuText.text = "What do you wish to use?";
            useButtons = new GameObject[playerItems];
            int curPos = 80;    // Y position of button, 0 is mid screen
            for (int i = 0; i < playerItems; i++)
            {
                int tempIndex = i;
                GameObject newButton = Instantiate(useListButton) as GameObject;
                newButton.transform.SetParent(useButtonBase, false);
                newButton.transform.localPosition = new Vector3(0, curPos - 35 * i, 0);
                useButtons[i] = newButton;
                newButton.GetComponentInChildren<Text>().text = player.GetItemInInventory(i).GetName();
                newButton.GetComponent<Button>().onClick.AddListener(() => itemChosen(tempIndex));
            }
        }
        
    }

    /// <summary>
    /// itemChosen attempts to use an item in player's inventory in an area. Player gets to choose from features in the area.
    /// </summary>
    /// <param name="itemIndex">Index of the item in player's inventory.</param>
    private void itemChosen(int itemIndex)
    {
        usingItem = player.GetItemInInventory(itemIndex);
        useMenuVisible = false;
        foreach (GameObject g in useButtons)
        {
            Destroy(g);
        }
        List<Feature> areaFeatures = gameMap.GetFeaturesAt(player.GetLocation()[0], player.GetLocation()[1]);
        if (areaFeatures.Count == 0)
        {
            addMessageToText("There is nothing to use " + player.GetItemInInventory(itemIndex).GetName() + " for.");
            useMenuBG.SetActive(false);
            useMenuText.gameObject.SetActive(false);
        }
        else if (!chooseFeatureVisible)
        {
            // Show buttons for features
            chooseFeatureVisible = true;
            useMenuText.text = "What do you wish to use it for?";
            useButtons = new GameObject[areaFeatures.Count];
            int curPos = 80;
            for (int i = 0; i < areaFeatures.Count; i++)
            {
                int tempIndex = i;
                GameObject newButton = Instantiate(useListButton) as GameObject;
                newButton.transform.SetParent(useButtonBase, false);
                newButton.transform.localPosition = new Vector3(0, curPos - 35 * i, 0);
                useButtons[i] = newButton;
                newButton.GetComponentInChildren<Text>().text = areaFeatures[i].GetName();
                newButton.GetComponent<Button>().onClick.AddListener(() => featureChosen(tempIndex));
            }
        }
        

    }

    /// <summary>
    /// featureChosen attempts to use an item in player's inventory with a feature in the area.
    /// </summary>
    /// <param name="index">Index of the feature in the area.</param>
    private void featureChosen(int index)
    {
        List<Feature> areaFeatures = gameMap.GetFeaturesAt(player.GetLocation()[0], player.GetLocation()[1]);
        chooseFeatureVisible = false;
        foreach (GameObject g in useButtons)
        {
            Destroy(g);
        }
        if (usingItem.CanUse(areaFeatures[index]))
        {
            string useString = "Using " + usingItem.GetName() + " with " + areaFeatures[index].GetName() + ". " + areaFeatures[index].UseFeature();

            if (useString.Contains("You escape!"))
            {
                completeGame(areaFeatures[index]);
            }

            if (useString.Contains("unlocks!"))
            {
                soundManager.PlaySound("unlock");
            }
            addMessageToText(useString);
            Item giveItem = areaFeatures[index].GetItem();
            if (giveItem != null)
            {
                player.AddItem(giveItem);
                addMessageToText("You find " + giveItem.GetName() + " in " + areaFeatures[index].GetName() + ".");
            }
            if (areaFeatures[index] is LockedDoor)
            {
                foreach (Image i in localSprites)
                {
                    if (i.sprite == lockedDoorSprite)
                    {
                        i.gameObject.SetActive(false);
                    }
                }
            }
            movesTaken++;

        }
        else
        {
            addMessageToText("You cant use " + usingItem.GetName() + " with " + areaFeatures[index].GetName() + ".");
        }
        useMenuBG.SetActive(false);
        useMenuText.gameObject.SetActive(false);
    }

    /// <summary>
    /// talk attempts to talk with another character. If other character is hostile player gets to choose if he fights with them. Otherwise player is presented with the talk screen.
    /// </summary>
    private void talk()
    {
        currentlyTalking = gameMap.GetNonPlayerAt(player.GetLocation()[0], player.GetLocation()[1]);
        if (currentlyTalking == null)
        {
            addMessageToText("There is no one to talk to.");
        }
        else if (currentlyTalking.Hostile)
        {
            fightChoise();
        }
        else
        {
            soundManager.PlaySound("encounter");
            useMenuBG.SetActive(true);
            useMenuText.gameObject.SetActive(true);
            dialogue.gameObject.SetActive(true);
            if (currentlyTalking.IsTrading())
            {
                talkYes.gameObject.SetActive(true);
                talkNo.gameObject.SetActive(true);
                tradeGives.gameObject.SetActive(true);
                tradeWants.gameObject.SetActive(true);
                tradeGives.sprite = currentlyTalking.TradeItem.ItemSprite;
                tradeWants.sprite = currentlyTalking.WantsInTrade.ItemSprite;
            }
            else
            {
                talkYes.gameObject.SetActive(false);
                talkNo.gameObject.SetActive(false);
                tradeGives.gameObject.SetActive(false);
                tradeWants.gameObject.SetActive(false);
            }
            useMenuText.text = currentlyTalking.GetName();
            dialogue.text = currentlyTalking.TalkWith();

        }
    }

    /// <summary>
    /// talkChoise handles player's choises during talking and trading.
    /// </summary>
    /// <param name="choise">String representing player's choice.</param>
    private void talkChoise(string choise)
    {
        switch(choise)
        {
            case "Yes":
                if (player.InventoryContains(currentlyTalking.WantsInTrade))
                {
                    player.RemoveItemFromInventory(currentlyTalking.WantsInTrade);
                    player.AddItem(currentlyTalking.TradeItem);
                    currentlyTalking.DoTrade();
                    addMessageToText(currentlyTalking.GetName() + " thanks you for the business.");
                    soundManager.PlaySound("trade");
                }
                else
                {
                    addMessageToText("Come back when you have <color=#00ff00>"
                        + currentlyTalking.WantsInTrade.GetName() + "</color>.");
                }
                break;
            case "No":
                addMessageToText("Come back when you wish to trade");
                break;
            case "Bye":

                break;
            default:
                break;
        }
        talkYes.gameObject.SetActive(false);
        talkNo.gameObject.SetActive(false);
        useMenuBG.SetActive(false);
        useMenuText.gameObject.SetActive(false);
        dialogue.gameObject.SetActive(false);
    }

    /// <summary>
    /// fightChoise asks if player wants to fight with another character.
    /// </summary>
    private void fightChoise()
    {
        soundManager.PlaySound("encounter");
        useMenuBG.SetActive(true);
        useMenuText.gameObject.SetActive(true);
        useMenuVisible = true;
        useMenuText.text = "Fight " + currentlyTalking.GetName() + "?";
        useButtons = new GameObject[2];
        string[] useTexts = { "Yes", "No" }; 
        int curPos = 80;    // Y position of button, 0 is mid screen
        for (int i = 0; i < 2; i++)
        {
            int tempIndex = i;
            GameObject newButton = Instantiate(useListButton) as GameObject;
            newButton.transform.SetParent(useButtonBase, false);
            newButton.transform.localPosition = new Vector3(0, curPos - 35 * i, 0);
            useButtons[i] = newButton;
            newButton.GetComponentInChildren<Text>().text = useTexts[i];
            newButton.GetComponent<Button>().onClick.AddListener(() => fightOptionChosen(tempIndex));
        }
    }

    /// <summary>
    /// fightOptionChosen executes players action after fight choice screen.
    /// </summary>
    /// <param name="choice">Integer representing player's choice.</param>
    private void fightOptionChosen(int choice)
    {
        foreach (GameObject g in useButtons)
        {
            Destroy(g);
        }
        useMenuBG.SetActive(false);
        useMenuText.gameObject.SetActive(false);
        useMenuVisible = false;
        if (choice == 0)
        {
            fightWith(currentlyTalking);
        }
        else
        {
            addMessageToText(currentlyTalking.GetName() + " glances at you angrily.");
        }
    }

    /// <summary>
    /// fightWith fights with another character. Player uses the best weapon in his inventory.
    /// </summary>
    /// <param name="opponent">The character the player is fighting with.</param>
    private void fightWith(Character opponent)
    {
        int playerPower = player.Strength;
        Item playerWeapon = player.GetBestWeapon();
        if (playerWeapon != null)
        {
            playerPower += playerWeapon.AttackPower;
        }

        int opponentPower = opponent.Strength;
        Item opponentWeapon = opponent.GetBestWeapon();
        if (opponentWeapon != null)
        {
            opponentPower += opponentWeapon.AttackPower;
        }

        if ( playerPower >= opponentPower )
        {
            if (opponent.GetName().Equals("Warden"))
            {
                completeGame(new Feature("Warden", ""));
            }
            foreach (Image i in localSprites)
            {
                if (i.sprite == currentlyTalking.GetSprite(0))
                {
                    i.gameObject.SetActive(false);
                }
            }
            string victoryMessagge = "You defeat " + currentlyTalking.GetName();
            List<Item> opponentInventory = currentlyTalking.GetWholeInventory();
            if (opponentInventory.Count > 0)
            {
                victoryMessagge += ". You find: ";
                foreach (Item i in opponentInventory)
                {
                    player.AddItem(i);
                    victoryMessagge += "<color=#00ff00>" + i.GetName() + "</color>, ";
                }
                victoryMessagge = victoryMessagge.Substring(0, victoryMessagge.Length - 2);
                victoryMessagge += ".";
            }
            gameMap.RemoveCharacter(currentlyTalking, player.GetLocation()[0], player.GetLocation()[1]);
            addMessageToText(victoryMessagge);
            soundManager.PlaySound("victory");

        }
        else
        {
            gameMap.MoveCharacter(player, 1, 7);
            player.SetLocation(1, 7);
            mainText.text = gameMap.GetDepiction(player.GetLocation()[0], player.GetLocation()[1]);
            areaBG.sprite = gameMap.GetBG(player.GetLocation()[0], player.GetLocation()[1]);
            drawLocalSprites(gameMap.GetPersonsAt(player.GetLocation()[0], player.GetLocation()[1]),
                            gameMap.GetAllItemsFromArea(player.GetLocation()[0], player.GetLocation()[1]),
                            gameMap.GetFeaturesAt(player.GetLocation()[0], player.GetLocation()[1]));
            movesTaken++;
            addMessageToText(currentlyTalking.GetName() + " beat you badly. You wake up in your cell.");
            soundManager.PlaySound("defeat");
        }
        
    }

    /// <summary>
    /// completeGame completes the game and loads the game over screen.
    /// </summary>
    /// <param name="escapeRoute">The feature player used to escape.</param>
    private void completeGame(Feature escapeRoute)
    {
        GameObject endInfo = (GameObject)Instantiate(EndInfoPrefab, Vector3.zero, Quaternion.identity);
        EndInfoScript endInfoScript = endInfo.GetComponent<EndInfoScript>();
        endInfoScript.EscapeRoute = escapeRoute;
        endInfoScript.MovesTaken = movesTaken;
        endInfoScript.Inventory = player.GetWholeInventory();
        if (escapeRoute.GetName().Equals("Warden"))
        {
            startEndImage.gameObject.SetActive(true);
            startEndButton.gameObject.SetActive(false);
            startEndText.gameObject.SetActive(false);
            StartCoroutine(fadeToBlack());
        }
        else
        {
            if (escapeRoute.GetName().Equals("Ventilation duct"))
            {
                soundManager.PlaySound("screwdriver");
            }
            else if (escapeRoute.GetName().Equals("Narrow hole"))
            {
                soundManager.PlaySound("shovel");
            }
            StartCoroutine(wait());
        }
    }

    /// <summary>
    /// fadeToBlack fades the screen black and plays the gunshot sound and then loads the game over screen.
    /// </summary>
    /// <returns>IEnumerator for coroutine and yield statement.</returns>
    private IEnumerator fadeToBlack()
    {
        float timeElapsed = 0.0f;
        float fadeTime = 0.8f;
        soundManager.PlaySound("gunshot");
        while (timeElapsed < fadeTime)
        {
            timeElapsed += Time.deltaTime;
            startEndImage.color = new Color(0, 0, 0, (timeElapsed / fadeTime));
            yield return 0;
        }
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(2);
    }

    /// <summary>
    /// wait waits for 1.5 seconds and the loads the game over screen.
    /// </summary>
    /// <returns>IEnumerator for coroutine and yield statement.</returns>
    private IEnumerator wait()
    {
        sliding = true;
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(2);
    }
}
