﻿
/// <summary>
/// Feature is the base class for all area features.
/// </summary>
public class Feature {

    private string name;
    private string description;
    private bool escapeRoute;
    private bool isLocked;
    private Area leadsTo;   // Shortcut
    private Item givesPlayer;

    /// <summary>
    /// Feature constructor takes two arguments.
    /// </summary>
    /// <param name="areaName">Name of the feature. Short string.</param>
    /// <param name="description">Description of the feature.</param>
    public Feature(string name, string description)
    {
        this.name = name;
        this.description = description;
        EscapeRoute = false;
        isLocked = true;
    }

    /// <summary>
    /// Get/Set for the area name.
    /// </summary>
    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }

    /// <summary>
    /// Get/Set for the isLocked boolean.
    /// </summary>
    public bool IsLocked
    {
        get { return isLocked; }
        set { isLocked = value; }
    }

    /// <summary>
    /// Get/Set for the escapeRoute boolean.
    /// </summary>
    public bool EscapeRoute
    {
        get { return escapeRoute; }
        set { escapeRoute = value; }
    }

    /// <summary>
    /// GetDescription returns the description of this feature.
    /// </summary>
    /// <returns>Description of this feature.</returns>
    public string GetDescription()
    {
        return this.description;
    }

    /// <summary>
    /// GetName returns the name of this feature.
    /// </summary>
    /// <returns>Name of this feature.</returns>
    public string GetName()
    {
        return this.name;
    }

    /// <summary>
    /// AddShortcut adds a shortcut to this feature.
    /// </summary>
    /// <param name="area">Area the shortcut leads to.</param>
    public void AddShortcut(Area area)
    {
        this.leadsTo = area;
    }

    /// <summary>
    /// SetGiveItem makes the feature give out an item when used with the correct tool item.
    /// </summary>
    /// <param name="i">Item this feature gives.</param>
    public void SetGiveItem(Item i)
    {
        this.givesPlayer = i;
    }

    /// <summary>
    /// UseFeature uses the feature.
    /// </summary>
    /// <returns>String describing what happens.</returns>
    public virtual string UseFeature()
    {
        if (!EscapeRoute)
        {
            IsLocked = false;

            return this.description;
        }
        else
        {
            return "You escape!";
        }
    }

    /// <summary>
    /// GetItem attempts to get an item from this feature.
    /// </summary>
    /// <returns>Item if this feature gives one, otherwise null.</returns>
    public Item GetItem()
    {
        if (this.givesPlayer != null)
        {
            Item tempItem = givesPlayer;
            givesPlayer = null;
            return tempItem;
        }
        else
        {
            return null;
        }
    }
	
}
